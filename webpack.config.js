const path = require('path');
const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  watch: false,
  mode: "development",
  devtool: "source-map",
  entry: path.resolve(__dirname, 'client/app.tsx'),
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'client/.dist'),
    publicPath: '/'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css']
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader'
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader'
    }, {
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      loader: 'url-loader',
      options: {
        limit: 10000
      }
    }]
  },
  optimization: {
    runtimeChunk: {
      name: "manifest"
    }
  },
  plugins: [
    new LiveReloadPlugin({})
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, 'client'),
    compress: true,
    port: 9000
  }
}
