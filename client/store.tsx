import { createStore, combineReducers, applyMiddleware, Store } from "redux";
import promiseMiddleware from 'redux-promise';
import { composeWithDevTools } from "redux-devtools-extension";
import { contactReducer, contactListReducer } from './main/contactReducer';
import { projectReducer, ProjectState } from './main/projectReducer';
import { ContactFormState } from "./main/contactTypes";

const rootReducer = combineReducers({
    contactForm: contactReducer,
    contacts: contactListReducer,
    projects: projectReducer
});

export type AppState = ReturnType<typeof rootReducer>;

const middlewares = [promiseMiddleware];
const middleWareEnhancer = applyMiddleware(...middlewares);

export const store: Store<{
  contactForm: ContactFormState,
  contacts: ContactFormState[],
  projects: ProjectState[]
}> = createStore(
  rootReducer,
  composeWithDevTools(middleWareEnhancer)
);