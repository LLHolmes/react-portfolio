import{ 
    ContactFormState, 
    UPDATE_CONTACT_FORM, 
    RESET_CONTACT_FORM
} from './contactTypes';
import { store } from '../store';

export function updateContactForm(updatedForm: ContactFormState) {
  store.dispatch({
    type: UPDATE_CONTACT_FORM,
    payload: updatedForm
  })
}

export function resetContactForm() {
    return {
      type: RESET_CONTACT_FORM
    };
}

// Start async action
export function sendContactForm(completeForm: ContactFormState) {
  console.log("SEND CONTACT FORM")
  console.log(completeForm)
}

export function addUpdateContact(contact: ContactFormState) {
  store.dispatch({
    type: "ADD_UPDATE_CONTACT",
    payload: contact
  })
}

export function removeContact(contact: ContactFormState) {
  store.dispatch({
    type: "REMOVE_CONTACT",
    payload: contact
  })
}