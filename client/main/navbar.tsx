import * as React from 'react';
import { NavLink as RRNavLink } from 'react-router-dom';
import Nav from 'reactstrap/lib/Nav';
import NavItem from 'reactstrap/lib/NavItem';
import NavLink from 'reactstrap/lib/NavLink';

export const NavBar: React.FunctionComponent<{}> = () => {

    return (
      <div className="NavBar">
        <Nav>
          <NavItem>
            <NavLink tag={RRNavLink} to="/">Home</NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={RRNavLink} to="/blog">Blog</NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={RRNavLink} to="/contact">Contact</NavLink>
          </NavItem>
        </Nav>
      </div>
    );
  
  }