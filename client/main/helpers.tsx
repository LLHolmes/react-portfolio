export const projectList = [
    {
        title: "Homebuyer's Helper", 
        summary: "Learn about the current real estate market to inform your home buying, selling, or investing experience using the Zillow API.", 
        description: "Home Sweet Homebuyer's Helper! Thinking of buy or selling a home? Want to keep tabs on your real estate investments? Need to contest the dreaded assessor’s office? /n Increase your knowledge of the current housing market with Homebuyer's Helper - a React app with a Rails back end. /n Enter an address into Homebuyer's Helper and receive information from the Zillow API on that home along with a list of recently sold comparable homes. /n If logged in, you can save the address of your home(s) and they'll greet you next time you log in. /n Happy home buying!",
        stack: {
            backend: 'Ruby on Rails', 
            frontend: 'React / Redux', 
            libraries: [
                'PostgreSQL: database',
                'Bcrypt: user authentication/authorization', 
                'Faraday: external API calls from Rails backend',
                'Redux Thunk: asynchronous calls', 
                'React Router: RESTful routes'
            ]},
        image: 'https://drive.google.com/file/d/1Txamy0IRuE1Ati54DPGKveBG9FzssXc5/view?usp=sharing',
        url: {
            app: 'https://homebuyers-helper-app.herokuapp.com/',
            demo: 'https://drive.google.com/file/d/1Ll339RDxYkevYRyb1CWfGlwTjr6c_LGc/view', 
            github: 'https://github.com/LLHolmes/relocation-helper.com'
        },
        example: {
            email: 'mr_prez@email.com', 
            password: 'password'
        }
    },
    {
        title: "Yarn Stash Organizer", 
        summary: "Wrangle your yarn stash to keep your crafty projects organized and under control.", 
        description: "Welcome to Yarn Stash Organizer, an application designed to help you wrangle your yarn stash and crafty projects. /n Add yarn to your virtual stash to track what yarn you have available for your crafting projects. Upon joining, you will automatically be given a Stash project which will be your default project to which all new yarn and tools will be added unless otherwise specified.  Your home page will display a list of your Projects, Yarn, and Tools. /n When you finish a project you will be taken to a secondary page where you should update the amount of yarn you have remaining to ensure your stash remains up to date. /n Happy hooking!",
        stack: {
            backend: 'Ruby on Rails', 
            frontend: 'JavaScript', 
            libraries: [
                'PostgreSQL: database',
                'Bcrypt: user authentication/authorization', 
                'OAuth: user login via Facebook',
                'jQuery: DOM manipulation'
            ]},
        image: 'https://drive.google.com/file/d/1mNJFCrlAF7QnKH0DEapdGx7NA3d-0zh2/view?usp=sharing',
        url: {
            app: 'https://yarn-stash-organizer.herokuapp.com/',
            demo: 'https://drive.google.com/file/d/19CSTl7rfUmX3PvtxGOXOf0ziJIrAHlPr/view', 
            github: 'https://github.com/LLHolmes/yarn_stash_organizer'
        },
        example: {
            email: 'gandalf@email.com', 
            password: 'password'
        }
    },
    {
        title: "Spellwork", 
        summary: "Explore spells from the Harry Potter universe.",
        description: "This Ruby Gem provides a Command Line Interface to view basic information on all known spells from the Harry Potter universe, from the Harry Potter Wiki on the [fandom website](https://harrypotter.fandom.com/wiki/List_of_spells).",
        stack: {
            cli: 'Ruby',
            libraries: [
                'Nokogiri & OpenURI: website scraping', 
                'Rainbow: console styling / color'
            ]},
        image: 'https://drive.google.com/file/d/1e1iGldFbbcGyrYJI4JFwLUBMtXS4FPTh/view?usp=sharing',
        url: {
            demo: 'https://drive.google.com/file/d/1DtHjRwZJ1w8RrGX4CBEv0efoIb6ukvQV/view', 
            github: 'https://github.com/LLHolmes/spellwork_cli'
        },
    }
];