import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { NavBar } from './navbar';
import { Blog } from './blog';
import Contact from './contact';
import { Portfolio } from './portfolio';


export const Main: React.FunctionComponent<{}> = () => {

  return (
    <div className="Main">
      <div>
        <NavBar />
      </div>
      <div>
        <Switch>
          <Route exact={true} path="/blog" component={Blog} />
          <Route exact={true} path="/contact" component={Contact} />
          <Route component={Portfolio} />
        </Switch>
      </div>
    </div>
  );

};
