import{ 
    ContactFormState, 
    UPDATE_CONTACT_FORM, 
    RESET_CONTACT_FORM, 
    ContactFormActionTypes
} from './contactTypes';
import { AnyAction } from 'redux';

const initialState: ContactFormState = {
    name: '',
    email: '',
    subject: '',
    message: ''
};
  
export function contactReducer(
    state = initialState, 
    action: ContactFormActionTypes
): ContactFormState {
    switch (action.type) {
        case UPDATE_CONTACT_FORM:
            return {...action.payload};
        case RESET_CONTACT_FORM:
            return {...initialState};
        default:
            return state;
    };
};


export function contactListReducer(
    state = [], 
    action: AnyAction) {
    switch (action.type) {
        case "ADD_UPDATE_CONTACT":
            state = [...state]
            const index = state.indexOf(action.payload)
            if (index === -1) {
                state.push(action.payload)
            } else {
                state[index] = action.payload
            }
            return state;
        case "REMOVE_CONTACT":
            return state.filter(contact => action.payload !== contact)
        default:
            return state;
    }
}