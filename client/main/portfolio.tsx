import * as React from 'react';
import { useProjects } from './hooks';
import Button from 'reactstrap/lib/Button'


export const Portfolio: React.FunctionComponent<{}> = () => {
  const projects = useProjects()

  return (
    <div className="Portfolio">
      <Button>Portfolio</Button>
      {projects.map((project, index) => <div key={index}>{project.title}</div>)}
    </div>
  );

}
