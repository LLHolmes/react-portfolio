export interface ContactFormState {
    name: string;
    email: string;
    subject: string;
    message: string;
};

export const UPDATE_CONTACT_FORM = 'UPDATE_CONTACT_FORM'
export const RESET_CONTACT_FORM = 'RESET_CONTACT_FORM'

export interface UpdateContactForm {
  type: typeof UPDATE_CONTACT_FORM
  payload: ContactFormState
}

export interface ResetContactForm {
  type: typeof RESET_CONTACT_FORM
}

export type ContactFormActionTypes = UpdateContactForm | ResetContactForm