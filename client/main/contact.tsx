import * as React from 'react';
import Button from 'reactstrap/lib/Button'
import Form from 'reactstrap/lib/Form';
import FormGroup from 'reactstrap/lib/FormGroup';
import Label from 'reactstrap/lib/Label';
import Input from 'reactstrap/lib/Input';
import { addUpdateContact } from "./contactActions";
import { useContacts } from './hooks';

const FormState = {
    name: '',
    email: '',
    subject: '',
    message: ''
}

const Contact: React.FunctionComponent<{}> = () => {
    const contacts = useContacts()

    const [myForm, setMyForm] = React.useState({...FormState})

    const handleChange = event => {
        setMyForm({
            ...myForm,
            [event.target.name]: event.target.value
        })
    };

    const handleSubmit = event => {
        addUpdateContact(myForm)
        setMyForm({...FormState})
    };

    return (
        <Form>
        <FormGroup>
            <Label for="formName">Name</Label>
            <Input type="text" name="name" value={myForm.name} id="formName" placeholder="your name" onChange={handleChange} />
        </FormGroup>
        <FormGroup>
            <Label for="formEmail">Email</Label>
            <Input type="email" name="email" value={myForm.email} id="formEmail" placeholder="your email" onChange={handleChange} />
        </FormGroup>
        <FormGroup>
            <Label for="formSubject">Subject</Label>
            <Input type="text" name="subject" value={myForm.subject} id="formSubject" placeholder="subject line" onChange={handleChange} />
        </FormGroup>
        <FormGroup>
            <Label for="formText">Your Message</Label>
            <Input type="textarea" name="message" value={myForm.message} id="formText" placeholder="your message" onChange={handleChange} />
        </FormGroup>
        <Button onClick={handleSubmit}>SUBMIT</Button>
        {contacts.map((contact, index) => <div key={index}>{contact.name}</div>)}
        </Form>
    );

};

export default Contact