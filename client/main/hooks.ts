import * as React from 'react'
import { store } from '../store'
import { ContactFormState } from './contactTypes'

export const useProjects = () => {
    const [projects, setProjects] = React.useState(
        store.getState().projects
    )

    React.useEffect(() => {
        const unsubscribe = store.subscribe(() => {
            const newProjects = store.getState().projects
            if (newProjects === projects) return
            setProjects(newProjects)
        })
        return () => unsubscribe()
    })

    return projects;
}

export const useContactForm = (): ContactFormState => {
    const [form, setForm] = React.useState(
        store.getState().contactForm
    )

    React.useEffect(() => {
        const unsubscribe = store.subscribe(() => {
            const newForm = store.getState().contactForm
            if (newForm === form) return
            setForm(newForm)
        })
        return () => unsubscribe()
    })

    return form;
}

export const useContacts = (): ContactFormState[] => {
    const [contacts, setContacts] = React.useState(
        store.getState().contacts
    )

    React.useEffect(() => {
        const unsubscribe = store.subscribe(() => {
            const newContacts = store.getState().contacts
            if (newContacts === contacts) return
            setContacts(newContacts)
        })
        return () => unsubscribe()
    })

    return contacts;
}