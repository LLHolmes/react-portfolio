import { AnyAction } from 'redux';
import { projectList } from './helpers'

export interface ProjectState {
    title: string; 
    summary: string;
    description: string;
    stack: {
        backend?: string;
        frontend?: string;
        cli?: string;
        libraries: string[];
    },
    image: string,
    url: {
        app?: string;
        demo: string;
        github: string;
    };
    example?: {
        email: string;
        password: string;
    }
};

const initialState = projectList
  
export function projectReducer(
    state = initialState, 
    action: AnyAction
) {
    switch (action.type) {
        default:
            return state;
    };
};