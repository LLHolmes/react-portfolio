import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from "react";
import * as ReactDom from "react-dom";
import { Provider } from 'react-redux';
import { BrowserRouter} from "react-router-dom";
import { Main } from './main/main';
import { store } from './store'

ReactDom.render(
    <BrowserRouter>
        <Provider store={store}>
            <Main />
        </Provider>
    </BrowserRouter>,
        document.getElementById("start"),
);
